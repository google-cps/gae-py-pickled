# Unpickling error with dev Datastore

Given an entity with an [`ndb.PickleProperty(compressed=True)`][0] and any other property type, editing the value of the other property using the Datastore Admin in dev and retrieving the entity raises an unpickling exception.

## Steps to reproduce using this repo

* `dev_appserver.py app.yaml` to start the dev server
* `curl localhost:8080/set` to create and store the entity
* Go to `localhost:8000/` to the Datastore Admin
* Modify the value of the `name` String property of the recently stored entity
* Flush memcache to ensure the next get retrieves the updated entity
* `curl localhost:8080/get` to retrieve the entity

For this error to occur, the pickle property must have `compressed=True`.  This does not happen when deployed to production.  There's no documentation indicating that `compressed=true` will raise errors in dev.

## Workaround

The following workaround works by not compressing a given `ndb.PickleProperty` when running in the development server.  This is really only effective when creating **new** local entities.  Existing entities with compressed pickle properties will still be compressed and still raise this unpickling error when modified by the local Datastore Admin.

```python
import os

in_production = os.getenv("SERVER_SOFTWARE", "").startswith("Google App Engine/")

class Example(ndb.Model):
    name = ndb.StringProperty()
    values = ndb.PickleProperty(compressed=in_production)
```

[0]: https://cloud.google.com/appengine/docs/python/ndb/entity-property-reference#types