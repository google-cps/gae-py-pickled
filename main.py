from google.appengine.ext import ndb

import webapp2

class Container(ndb.Model):
    name = ndb.StringProperty()
    values = ndb.PickleProperty(compressed=True)

unique_key_name = "UUID"
    
class Getter(webapp2.RequestHandler):
    def get(self):
        self.response.content_type = "text/plain"
        
        key = ndb.Key(Container, unique_key_name)

        try:
            entity = key.get()
        except Exception as e:
            self.response.write("Could not retrieve entity {}\n{}\n".format(unique_key_name, e))
            return

        if entity is None:
            self.response.write("Try /set first since there is no saved entity\n")
            return

        self.response.write("Retrieved entity {}\n".format(unique_key_name))
        self.response.write("  Entity name is '{}'\n".format(entity.name))
        for v in entity.values:
            self.response.write("  Found value {} in entity {}\n".format(v, unique_key_name))

class Setter(webapp2.RequestHandler):
    def get(self):
        self.response.content_type = "text/plain"

        entity = Container(id=unique_key_name, name="Sam and Max", values=[12, 0, 42])

        try:
            key = entity.put()
        except Exception as e:
            self.response.write("Could not set entity {}\n{}\n".format(unique_key_name, e))
            return

        self.response.write("Set entity correctly with id {}\n".format(unique_key_name))
        self.response.write("  Entity name is '{}'\n".format(entity.name))
        for v in entity.values:
            self.response.write("  Found value {} in entity {}\n".format(v, unique_key_name))

app = webapp2.WSGIApplication([
    ("/set", Setter),
    ("/get", Getter),
], debug=True)
